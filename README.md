# Corineus

Corineus is a wrapper for the `kinit` and `nsupdate` commands to allow
easy authenticated updates of DNS records on a Microsoft Windows DNS server from
Linux.

## Installation

### CentOS 7

```bash
# Dependencies
sudo yum -y install rubygems krb5-workstation bind-utils

# Install the gem
gem install corineus
```

### Ubuntu 14.04 (Trusty)

```bash
# Dependencies
sudo DEBIAN_FRONTEND=noninteractive apt-get -yq install krb5-user dnsutils ruby

# Install the gem
gem install corineus
```

## Usage

### Add

```
  corineus add -d, --data=Data for the record -k, --kdc=Kerberos KDC -n, --name=Record to be added -p, --password=Authorized password  -r, --realm=Kerberos realm -s, --server=DNS server -u, --user=Authorized username

Options:
  -n, --name=Record to be added
  -d, --data=Data for the record
  -t, [--type=Type of record]
                                                  # Default: A
  -l, [--ttl=Time To Live]
                                                  # Default: 86400
  -s, --server=DNS server
  -r, --realm=Kerberos realm
  -k, --kdc=Kerberos KDC
  -u, --user=Authorized username
  -p, --password=Authorized password
  -v, [--verbose=Verbose output], [--no-verbose]

Description:
  `corineus add` will add a DNS record to a server, authenticating via Kerberos.

  > $ corineus add --name=www.mydomain.com --data=server1.mydomain.com --type=CNAME --server=dns1.mydomain.com --realm=MYDOMAIN.COM --kdc dc1.mydomain.com --user=admin --password=mypass
```

### Delete

```
  corineus delete -k, --kdc=Kerberos KDC -n, --name=Record to be deleted -p, --password=Authorized password  -r, --realm=Kerberos realm -s, --server=DNS server -u, --user=Authorized username

Options:
  -n, --name=Record to be deleted
  -s, --server=DNS server
  -r, --realm=Kerberos realm
  -k, --kdc=Kerberos KDC
  -u, --user=Authorized username
  -p, --password=Authorized password
  -v, [--verbose=Verbose output], [--no-verbose]

Description:
  `corineus delete` will delete a DNS record from a server, authenticating via Kerberos.

  > $ corineus delete --name=www.mydomain.com --server=dns1.mydomain.com --realm=MYDOMAIN.COM --kdc dc1.mydomain.com --user=admin --password=mypass
```

## Contributing

Bug reports and pull requests are welcome on GitLab at https://gitlab.com/harbottle/corineus.


## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
