# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'corineus/version'

Gem::Specification.new do |spec|
  spec.name          = "corineus"
  spec.version       = Corineus::VERSION
  spec.authors       = ["Richard Grainger"]
  spec.email         = ["grainger@gmail.com"]

  spec.summary       = "corineus - update DNS records on remote Windows " \
    "servers from Linux"
  spec.description   = "Corineus is a wrapper for the kinit and nsupdate " \
    "commands to allow easy authenticated updates of DNS records on a " \
    "Microsoft Windows DNS server from Linux."
  spec.homepage      = "https://gitlab.com/harbottle/corineus"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "bin"
  spec.executables   << "corineus"
  spec.require_paths << "lib"

  spec.add_dependency("colorize", ">=0.8.1")
  spec.add_dependency("POpen4", ">=0.1.4")
  spec.add_dependency("thor", ">=0.19.1")

  spec.add_development_dependency "bundler", "~> 1.12"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "minitest", "~> 5.0"
end
