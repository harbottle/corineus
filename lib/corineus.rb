require "corineus/version"
require 'thor'
require 'tempfile'
require 'popen4'
require 'colorize'

class String
  def unindent 
    gsub(/^#{scan(/^\s*/).min_by{|l|l.length}}/, "")
  end
end

class CORINEUS < Thor

  class_option :verbose, :banner => 'Verbose output', :type => :boolean, :default  => false, :aliases  => '-v'

  desc "add", "Add a DNS record"
  long_desc <<-LONGDESC
    `corineus add` will add a DNS record to a server, authenticating via
    Kerberos.

    > $ corineus add --name=www.mydomain.com --data=server1.mydomain.com
    --type=CNAME --server=dns1.mydomain.com --realm=MYDOMAIN.COM
    --kdc dc1.mydomain.com --user=admin --password=mypass
  LONGDESC
  option :name,     :banner => 'Record to be added',   :required => true,    :aliases  => '-n'
  option :data,     :banner => 'Data for the record',  :required => true,    :aliases  => '-d'
  option :type,     :banner => 'Type of record',       :default  => 'A',     :aliases  => '-t'
  option :ttl,      :banner => 'Time To Live',         :default  => '86400', :aliases  => '-l'
  option :server,   :banner => 'DNS server',           :required => true,    :aliases  => '-s'
  option :realm,    :banner => 'Kerberos realm',       :required => true,    :aliases  => '-r'
  option :kdc,      :banner => 'Kerberos KDC',         :required => true,    :aliases  => '-k'
  option :user,     :banner => 'Authorized username',  :required => true,    :aliases  => '-u'
  option :password, :banner => 'Authorized password ', :required => true,    :aliases  => '-p'
  
  def add()
   update 'add'
  end

  desc "delete", "Delete a DNS record"
  long_desc <<-LONGDESC
    `corineus delete` will delete a DNS record from a server, authenticating via
    Kerberos.

    > $ corineus delete --name=www.mydomain.com  --server=dns1.mydomain.com
    --realm=MYDOMAIN.COM --kdc dc1.mydomain.com --user=admin --password=mypass
  LONGDESC
  option :name, :banner   => 'Record to be deleted', :required => true, :aliases  => '-n'
  option :server, :banner   => 'DNS server', :required => true, :aliases  => '-s'
  option :realm, :banner   => 'Kerberos realm', :required => true, :aliases  => '-r'
  option :kdc, :banner   => 'Kerberos KDC', :required => true, :aliases  => '-k'
  option :user, :banner   => 'Authorized username', :required => true, :aliases  => '-u'
  option :password, :banner   => 'Authorized password ', :required => true, :aliases  => '-p'

  def delete()
    update 'delete'
  end

  no_commands do
    def update(action='add')

      k_conf = <<-KRBCONF.unindent
      [realms]
      #{options[:realm]} = {
      kdc = #{options[:kdc]}
      default_domain = #{options[:realm]}
      }
      KRBCONF

      ns_input = <<-NSINPUT.unindent
      server #{options[:server]}
      update #{action} #{options[:name]} #{options[:ttl]} #{options[:type]} #{options[:data]}
      send
      quit
      NSINPUT

      pad_length = 50
      k_file = Tempfile.new('krb5')
      k_file.write(k_conf)
      k_file.rewind

      commands = {
        "Kerberos" => {
          :desc    => 'Get Kerberos ticket',
          :command => "env KRB5_CONFIG=#{k_file.path} kinit #{options[:user]}@#{options[:realm]}",
          :input   => "#{options[:password]}",
          :exit    => 1,
          :clue    => 'Do you have the kinit command installed and in your PATH?'
        },
        "nsupdate" => {
          :desc    => "#{action.capitalize} DNS record",
          :command => "env KRB5_CONFIG=#{k_file.path} nsupdate -g",
          :input   => ns_input,
          :exit    => 2,
          :clue    => 'Do you have the nsupdate command installed and in your PATH?'
        }
      }
      commands.each do |task, details|
        outputs = ''
        errors =  ''
        print "#{details[:desc]}...".ljust(pad_length, padstr='.') if options[:verbose]
        command_status = POpen4::popen4(details[:command]) do |stdout, stderr, stdin|
          stdin.puts details[:input]
          stdout.each do |line|
            outputs << "#{line.strip}\n"
          end
          stderr.each do |line|
            errors << "#{line.strip}\n"
          end
        end
        if command_status.exitstatus == 0
          print "Success.\n".green if options[:verbose]
        else
          if options[:verbose]
            print "Failure:\n".red
            print "#{outputs.strip}\n".red
            print "#{errors.strip}\n".red
            print "#{details[:clue]}\n".light_blue
          end
          exit details[:exit]
        end
      end

      k_file.close
      k_file.unlink
    end
  end

end

